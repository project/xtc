# Load content from Profile

## Profile loader

### Load Profile

Whatever the profile, content should be always loaded the same way.

```
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;

// Load plain profile
public function test() {
    $this->content = XtcLoaderProfile::content('profile_name');
    return New JsonResponse($this->content);
}

// Load profile with options
public function test() {
    $options = [];
    $this->content = XtcLoaderProfile::content('profile_name', $options);
    return New JsonResponse($this->content);
}
```

### Change profile, not code

The code structure allows to switch source without changing code: just change 
the Profile definition from `my_module.xtc_profiles.yaml`.

From a Json file:

```
hello:
  label: 'Json Demo'
  description: 'A Json content stored in a local file at modules/custom/my_module/demo/hello.json.'
  type: 'json'
  verb: 'get'
  abs_path: false
  module: 'my_module'
  path: 'demo/hello.json'
```

To a Guzzle web service:

```
hello:
  label: 'Hello web service'
  description: 'A webservice named Hello.'
  type: 'guzzle'
  verb: 'get'
  server: 'ws_server'
  method: 'hello'
```

## Server loader

### Define Server


The servers are defined in `my_module.servers.yaml` where multiple 
environments can be defined, and active env must be specified.

```
ws_server:
  label: 'Server'
  description: 'Some Webservice Server.'
  type: 'guzzle'
  env: 'prod'
  options:
    verify: false
    timeout: 5
  connection:
    dev:
      tls: false
      host: "ws.example.local"
      port: 80
      endpoint: "api"
    int:
      tls: true
      host: "ws.int.example.com"
      port: 5443
      endpoint: "api"
    prod:
      tls: true
      host: "ws.example.com"
      port: 443
      endpoint: "api"
```

`type` could be any available **XtcServer** plugin name. Such as `guzzle` or `elastica` (for ElasticSearch).

### Manage environments

Define as many environments as needed. 

### Define active environments

The `env` value can be overriden in the `*.settings.php` at environment level.

