# Override Profile definition

The `recipe` Profile provide a generic definition that is overriden by more 
specific profiles. 

Profiles can be overriden iteratively. This helps make sure Profiles definitions 
are kept consistent. 

```
### Test for Schema.org ###
recipe:
  label: 'Recipe from Schema.org'
  description: 'Recipe from Schema.org'
  type: 'recipe'
  verb: 'get'
  schema: 'Recipe'

tatin:
  label: 'Tarte Tatin'
  description: 'Recette de la tarte tatin depuis 750g, au format schema.org'
  override: 'recipe'
  fullurl: 'https://www.750g.com/tarte-tatin-r100132.htm'

tarte:
  label: 'Tarte aux pommes'
  description: 'Recette de la tarte aux pommes depuis 750g, au format schema.org'
  override: 'recipe'
  fullurl: 'https://www.750g.com/tarte-aux-pommes-r99700.htm'
```
