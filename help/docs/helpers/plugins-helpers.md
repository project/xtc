# Plugins Helpers

Many Plugins Helpers can be found in the `Drupal\xtc\XtendedContent\API` namespace:

## Loaders

### Abstract class

Loaders provides functions that facilitate access to XTC plugins.

`XtcLoaderBase` is the abstract classes used to build a Loader class. It provides :

- `getList()`: list plugins definitions
- `get()`: returns the plugin object
- `load()`: returns the plugin definition
- `getService()`: returns the name of the service.

### Provided loaders

Each Plugin-Manager provided by Xtended Content suite should have a `XtcLoader*` class based
on `XtcLoaderBase`.

- `XtcLoaderServer`: `XtcServerDefault`
- `XtcLoaderProfile`: `XtcProfileDefault`
- `XtcLoaderHandler`: `XtcHandlerPluginBase`
- `XtcLoaderCache`: `XtcCachePluginBase`





## Classes extending `PluginBase`

They provide `get()` and `load()` functions.

- `XtcSearchDisplay`
- `XtcSearchFilter`
- `XtcSearchFilterType`
- `XtcForm`
- `XtcHandler`
- `XtcElasticaMapping`
- `XtcSearchPager`
- `XtcProfile`
- `XtcRequest`
- `XtcServer`

### Extending the Helpers

Providing a new Helper class can be done that way:

```php
namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtcsearch\PluginManager\XtcSearchDisplay\XtcSearchDisplayDefault;

class XtcSearchDisplay extends PluginBase
{

  public static function get($name): XtcSearchDisplayDefault{
    return parent::get($name);
  }

  protected static function getService() : string {
    return 'plugin.manager.xtcsearch_display';
  }
}
```

- Make sure to define the right return type to the `get()` function.
- Define the Plugin Manager service in the `getService()` function.

## ToolBox

The `ToolBox` Helper class is used to provide Xtended Content suite with plain tool static functions.

For now:

- `replaceAccents()`
- `transliterate()`
- `toCssClass()`
- `buildId()`
- `getPrefix()`
- `getSuffix()`
- `getPlugins()`
- `getPluginDefinitions()`
- `splitPipe()`
