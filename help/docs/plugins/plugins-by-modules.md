# Plugins by modules

4 plugins managers are defined throughout the XTC modules.

## Active modules

### xtc module

4 plugin managers are defined in `xtc`.

#### 4 active plugin managers 

- `xtc_handler`
- `xtc_profile` 
- `xtc_server` 
- `xtc_cache` 

### xtcdrupal module

No plugin manager is defined in `xtcdrupal`.

### xtcfile module

No plugin manager is defined in `xtcfile`.

### xtcguzzle module

No plugin manager is defined in `xtcguzzle`.
