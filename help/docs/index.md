# Xtended Content suite

[Xtended Content](https://www.drupal.org/project/xtc) is a suite of modules that 
provides access to external content from Drupal 8.

- [About](/about/technical-vision)
- [Modules](/modules/xtc)
- [Using XTC](/using-xtc/load-content)
- [Plugins](/plugins/plugins-by-modules)
- [Helpers](/helpers/plugins-helpers)
- [Coding XTC](/coding-xtc/handler)

## Code only

For now, it's a code only approach that provides an API, but no administration interface. 

> As a consequence, XTC can not be used without creating a custom module. 

## Example — Drupal as a File CMS

Let's say that, while complex contents are managed through usual Drupal entities, you would like to integrate 
a few (or many) markdown files into the project: you just don't want to copy-paste 1346 markdown files into 1346 
articles because you've got better things to do in your life :-)

A minimum custom module named **my_xtc** would be made of the usual minimum structure:

- a plain `my_xtc.info.yml` file, and
- a dummy `my_xtc.module` file with `hook_help()`
- a `README.md` is always appreciated ;-) 
- and a `composer.json` would be wonderful :-D 

Coming to XTC needs, to provide a markdown file as your first resource:

- content files in a `content/` directory
- a simple `my_xtc.module` file that would look like: 

``` 
test_md:
  label: 'MarkDown file'
  description: ''
  type: 'markdown'
  verb: 'get'
  abs_path: false
  module: 'my_xtc'
  path: 'content/demo.md'
```

That's it.

## A profile per file, seriously?

Yes, you can also define a profile for a directory of content files, or from a webservice, or from ElasticSearch. 
But that would be different examples.

> In fact, the whole idea about XTC, is to be able to change the resource from a Json file to a REST webservice 
> without changing a single line of code.

**Stop migrating, start plugging**
