# Principles

## Stop format-locking

From the origins of Drupal, contents are stored in the database to benefit from 
very advanced toolbox like Views, CTools and Paragraphs.

Xtended Content (or **XTC**) provides an abstract layer to help Drupal 8 access 
data that are not stored in the D8 database, and display it directly.

## Don't change the code

As a start, XTC is helping Drupal developers to easily connect their Drupal 
projects to a large diversity of sources in a solid way:

- Define a Profile (a Yaml D8 plugin), create a Route, and load Data from a plain 
controller to display it through a Drupal `hook_theme()` defined destination 
(or a `JsonResponse`).
- Change the definition of your Profile (4 or 6 lines in your Yaml file), don't 
touch the code.
- That's it! You've just switched from a Json file to your permanent Guzzle 
webservice.

## CI/CD oriented

Server plugin-manager allows to define as many environments as needed for a given 
Server. Just set the name fo the current environment (at server level) in the 
`settings.local.php` file.

## Make your own handlers

Here are some examples of Data handlers XTC already manages:

- Files and Directories of files: csv, Html, Json, Markdown, Readme, MkDocs, Text, Yaml.
- Webservices through Guzzle
- Metadata parsed from webpages with Micrometa
- Elasticsearch
- Drupal local API

## CRUD is not an option

We are not only expecting to read, but also to provide CRUD features. To do this, 
Profiles plugins are defined by types and verbs: `file_delete` can be defined to 
code a plugin based on the File handler, and helping to delete... a file.

## Caching workflow

**Cache** plugin-manager was created to help define workflow to easily obtain 
better performance. A simple case:

- Parse a Recipe data from 750g web page and normalize it to display it in a 
given template.
- Store it in a local page, not to have to connect to 750g website each time you 
want to make your prefered Apple crumble.
- Test if the local file exists, and fetch only on fail.
- Create a Memcache handler, change cache profile to a memcache definition, and 
it will be faster. Change it to an Elasticsearch profile, and it will be more fun.

We shouldn't have to change any existing business code for that.
