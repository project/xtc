# Modules that need work

- [Xtended Content Elastica](https://www.drupal.org/project/xtcelastica)
- [Xtended Content Search](https://www.drupal.org/project/xtcsearch)

These modules need to be updated. So they are not covered by the current 
documentation.

## XTC Elastica

Provides access to ElasticSearch using the Elastica library. It uses a legacy 
API that needs to be updated.

> To access ElasticSearch data, please consider using **XTC PHP ElasticSearch** 
> that is based on the **PHP Elasticsearch** library, and is up to date.

## XTC Search

Provides a full search UI experience with dynamic facets, but is based on 
**XTC Elastica**.  So it needs to be updated too.

No alternative based on **XTC PHP ElasticSearch** is presently provided.

## Deprecated code

### `API\Config`

Helper static functions defined in `\Drupal\xtc\XtendedContent\API\Config` are 
considered deprecated.

### `Serve\Client` & `Serve\XtcRequest`

- Classes defined in `\Drupal\xtc\XtendedContent\Serve\Client` are considered 
deprecated.
- Classes defined in `\Drupal\xtc\XtendedContent\Serve\XtcRequest` are considered 
deprecated.

This is mostly first version code. Cleanup is not over. It should only concern 
**XTC Elastica** & **XTC Search** modules. These need to be updated and are not 
covered by present documentation.