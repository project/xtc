# Technical vision

The golden features in Drupal is the fact that it is a Content Centric CMS. 
As time goes by, while dealing with websites that are bigger, more complex and 
more advanced, it appears more and more that reality is in fact: a Drupal database 
Content Centric CMS.

As the Web is going more and more complex and interconnected, it starts to appear 
as a limitation. Ten years ago, we started to build bigger and more complex 
websites: Drupal 7 was just great for that. From now on, RSS ans Schema are not 
enough: API web is the key word. And migrating data is always a project in the 
project, a bigger risk for planning and money.

From our point of view, we should stop migrating data, and start plugging it. 
That's it.
