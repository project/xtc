# Architecture

## Helpers

Many Helper functions can be found in the `XtendedContent/API` directory in `xtc` 
module.

## Plugins

4 plugins-managers are defined throughout the XTC modules.

- Yaml plugins are used when the Default class is mostly used, and plugins are 
data driven.
- Annotation plugins are used when plugins are code driven.
