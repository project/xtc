# To be done

`@todo` tags are used in the project to identify what is to be done.

We recommend using tools like PhpStorm **TODO** panel to help track these tasks 
directly in the deep code.