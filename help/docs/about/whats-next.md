# What's next?

These are examples of the roadmap:

- Propose plugin managers similar to Cache for purposes like Mirror and Balance.
- Complete local Drupal API as much as possible: more popular entities, and some 
basic views handlers would be a good start.
- Dedicated modules to Drupal Rest API and WordPress Rest API: this would help 
us to build complex workflow architectures with simple nodes, with a strong 
dedicated business, and really easy to maintain. An Akineo Rest API module could 
be great too! Nuxeo, anyone?
- A new version of XTC Search based on the new API.
- A Dummy entity that will eventually bring us back to the real Drupal world: 
widgets, displays, formatters, and, as a consequence: Views!

If you believe this is challenging and fun: come aboard!
