# Main modules

- [Xtended Content](https://www.drupal.org/project/xtc): API module
- [Xtended Content Drupal](https://www.drupal.org/project/xtcdrupal)
- [Xtended Content File](https://www.drupal.org/project/xtcfile)
- [Xtended Content Guzzle / ReST](https://www.drupal.org/project/xtcguzzle)
- [Xtended Content Schema.org](https://www.drupal.org/project/xtcschema)
- [Xtended Content PHP ElasticSearch](https://www.drupal.org/project/xtcelasticsearch)
