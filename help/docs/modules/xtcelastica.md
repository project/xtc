# Xtended Content Elastica (xtcelastica)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtcelastica](https://www.drupal.org/project/xtcelastica)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtcelastica](https://gitlab.com/sashainparis/xtcelastica)
- Issues: [https://gitlab.com/sashainparis/xtcelastica/issues](https://gitlab.com/sashainparis/xtcelastica/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtcelastica/merge_requests](https://gitlab.com/sashainparis/xtcelastica/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtcelastica/docs`
