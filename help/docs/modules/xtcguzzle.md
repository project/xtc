# Xtended Content Guzzle / ReST (xtcguzzle)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtcguzzle](https://www.drupal.org/project/xtcguzzle)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtcguzzle](https://gitlab.com/sashainparis/xtcguzzle)
- Issues: [https://gitlab.com/sashainparis/xtcguzzle/issues](https://gitlab.com/sashainparis/xtcguzzle/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtcguzzle/merge_requests](https://gitlab.com/sashainparis/xtcguzzle/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtcguzzle/docs`
