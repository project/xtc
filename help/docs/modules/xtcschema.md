# Xtended Content Schema.org (xtcschema)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtcschema](https://www.drupal.org/project/xtcschema)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtcschema](https://gitlab.com/sashainparis/xtcschema)
- Issues: [https://gitlab.com/sashainparis/xtcschema/issues](https://gitlab.com/sashainparis/xtcschema/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtcschema/merge_requests](https://gitlab.com/sashainparis/xtcschema/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtcschema/docs`
