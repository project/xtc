# Xtended Content File (xtcfile)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtcfile](https://www.drupal.org/project/xtcfile)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtcfile](https://gitlab.com/sashainparis/xtcfile)
- Issues: [https://gitlab.com/sashainparis/xtcfile/issues](https://gitlab.com/sashainparis/xtcfile/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtcfile/merge_requests](https://gitlab.com/sashainparis/xtcfile/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtcfile/docs`
