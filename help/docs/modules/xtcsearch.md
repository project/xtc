# Xtended Content Search (xtcsearch)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtcsearch](https://www.drupal.org/project/xtcsearch)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtcsearch](https://gitlab.com/sashainparis/xtcsearch)
- Issues: [https://gitlab.com/sashainparis/xtcsearch/issues](https://gitlab.com/sashainparis/xtcsearch/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtcsearch/merge_requests](https://gitlab.com/sashainparis/xtcsearch/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtcsearch/docs`
