# Xtended Content (xtc)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtc](https://www.drupal.org/project/xtc)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtc](https://gitlab.com/sashainparis/xtc)
- Issues: [https://gitlab.com/sashainparis/xtc/issues](https://gitlab.com/sashainparis/xtc/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtc/merge_requests](https://gitlab.com/sashainparis/xtc/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtc/docs`
