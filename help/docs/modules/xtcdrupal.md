# Xtended Content Drpual (xtcdrupal)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtcdrupal](https://www.drupal.org/project/xtcdrupal)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtcdrupal](https://gitlab.com/sashainparis/xtcdrupal)
- Issues: [https://gitlab.com/sashainparis/xtcdrupal/issues](https://gitlab.com/sashainparis/xtcdrupal/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtcdrupal/merge_requests](https://gitlab.com/sashainparis/xtcdrupal/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtcdrupal/docs`
