# Xtended Content PHP ElasticSearch (xtcelasticsearch)

## On Drupal.org

- Project page: [https://www.drupal.org/project/xtcelasticsearch](https://www.drupal.org/project/xtcelasticsearch)

## On Gitlab

- Repository: [https://gitlab.com/sashainparis/xtcelasticsearch](https://gitlab.com/sashainparis/xtcelasticsearch)
- Issues: [https://gitlab.com/sashainparis/xtcelasticsearch/issues](https://gitlab.com/sashainparis/xtcelasticsearch/issues)
- Pull-Requests: [https://gitlab.com/sashainparis/xtcelasticsearch/merge_requests](https://gitlab.com/sashainparis/xtcelasticsearch/merge_requests)

## Documentation

Documentation for the whole Xtended Content suite: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)

- Documentation is published at: [https://xtc.docs.sasha.paris](https://xtc.docs.sasha.paris)
- Documentation is managed in the XTC module: `/help/docs` ([MkDocs](https://www.mkdocs.org/) format)
- Documentation can be accessed directly inside the Drupal administration area:<br />
`/admin/documentation/xtcelasticsearch/docs`
