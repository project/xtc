<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 16:47
 */

namespace Drupal\xtc\XtendedContent\API;


abstract class WriterBase
{

  /**
   * @param $content
   * @param $name
   * @param array $options
   *
   * @return mixed
   */
  abstract public static function write($content, $name, $options = []);

}
