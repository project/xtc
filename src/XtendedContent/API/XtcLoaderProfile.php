<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;

use Drupal\Core\Site\Settings;
use Drupal\xtc\PluginManager\XtcProfile\XtcProfileDefault;
use Drupal\xtcprofile\Entity\XtcProfile;

class XtcLoaderProfile extends XtcLoaderBase {

  public static function get($name, $options = []): XtcProfileDefault {
    return parent::get($name);
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase|null
   */
  public static function process($name, $options = []) {
    $handler = self::loadHandler($name, $options);
    if (!empty($handler)) {
      return $handler->process();
    }
    return NULL;
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return string|null
   */
  public static function delete($name, $options = []) {
    $handler = self::loadHandler($name, $options);
    if (!empty($handler)) {
      return $handler->process();
    }
    return NULL;
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return mixed|null
   */
  public static function content($name, $options = []) {
    $handler = self::loadHandler($name, $options);
    if (!empty($handler)) {
      return $handler->processContent();
    }
    return NULL;
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return mixed|null
   */
  public static function values($name, $options = []) {
    $handler = self::loadHandler($name, $options);
    if (!empty($handler)) {
      return $handler->processValues();
    }
    return NULL;
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return mixed|null
   */
  public static function filters($name, $options = []) {
    $handler = self::loadHandler($name, $options);
    if (!empty($handler)) {
      return $handler->processFilters();
    }
    return NULL;
  }

  protected static function getService(): string {
    return 'plugin.manager.xtc_profile';
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase|null
   */
  public static function loadHandler($name, $options = []) {
    $profile = self::load($name);
    if (!empty($profile)) {
      if (!empty($profile['args'])) {
        $options = array_merge($profile['args'], $options);
      }
      if (!empty($profile['handler'])) {
        $handler = XtcLoaderHandler::get($profile['handler']);
      }
      elseif (!empty($profile['type']) && !empty($profile['verb'])) {
        $handler = XtcLoaderHandler::get($profile['type'] . '_' . $profile['verb']);
      }
      $options = array_merge($profile, $options);
      return $handler->setProfile($profile)
        ->setOptions($options);
    }
    return NULL;
  }

  /**
   * @inheritDoc
   */
  protected static function loadXtcEntity($name): array {
    $xtcloader = XtcProfile::load($name);
    $definition = [];
    $options = $xtcloader->get('options');

    $definition['label'] = $xtcloader->label();
    $definition['id'] = $xtcloader->id();
    $definition['description'] = $options['description'];
    $definition['handler'] = $options['handler'];

    $xtcHandler = XtcLoaderHandler::get($options['handler']);
    foreach ($xtcHandler::getStruct() as $field) {
      switch($field) {
        case 'path_root':
          if (!empty($options[$field])) {
            switch ($options['path_root']) {
              case 'abs_path':
                $definition['abs_path'] = TRUE;
                break;
              case 'flysystem':
              case 'public':
                $definition['abs_path'] = FALSE;
                $definition['path_root'] = $options['path_root'];
                break;
              default:
                $definition['abs_path'] = FALSE;
                $definition['module'] = $options['path_root'];
            }
          }
          break;
        case 'token':
          if (!empty($options['token_type']) && !empty($options['token_value']) ) {
            $definition['token'] = [
              'type' => $options['token_type'],
              'value' => $options['token_value'],
            ];
          }
          break;
        default:
          if (!empty($options[$field])) {
            $definition[$field] = $options[$field];
          }
      }
    }
    return $definition;
  }

}
