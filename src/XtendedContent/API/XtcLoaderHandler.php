<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

class XtcLoaderHandler extends XtcLoaderBase {

  /**
   * @param $name
   * @param array $options
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public static function get($name, $options = []): XtcHandlerPluginBase {
    if (!empty($options)) {
      return self::getHandlerFromProfile($name, $options);
    }
    return parent::get($name);
  }

  /**
   * @return string
   */
  protected static function getService(): string {
    return 'plugin.manager.xtc_handler';
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase|null
   */
  public static function getHandlerFromProfile($name, $options = []) {
    $profile = XtcLoaderProfile::load($name);
    if (!empty($profile)) {
      return self::get($profile['type'] . '_' . $profile['verb'])
        ->setProfile($profile)
        ->setOptions($options);
    }
    return NULL;
  }

}
