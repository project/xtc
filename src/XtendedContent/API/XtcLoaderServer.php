<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtc\PluginManager\XtcServer\XtcServerDefault;
use Drupal\xtcserver\Entity\XtcServer;

class XtcLoaderServer extends XtcLoaderBase
{

  const XtcServerStruct = [
    'env_names' => ['dev', 'int', 'test', 'training', 'pprod', 'prod'],
    'env_fields' => ['label', 'id', 'tls', 'host', 'port', 'endpoint'],
  ];

  public static function get($name, $options = []): XtcServerDefault{
    return parent::get($name);
  }

  protected static function getService() : string {
    return 'plugin.manager.xtc_server';
  }

  /**
   * @inheritDoc
   */
  protected static function loadXtcEntity($name): array {
    $xtcloader = XtcServer::load($name);
    $definition = [];
    $options = $xtcloader->get('options');
    $type = $options['type'];

    $definition['label'] = $xtcloader->label();
    $definition['id'] = $xtcloader->id();
    $definition['env'] = $options['env'];
    $definition['description'] = $options['description'];
    $definition['type'] = $type;

    foreach (self::XtcServerStruct['env_names'] as $name) {
      if($options['enabled_' . $name]) {
        if($options['self_' . $name]) {
          $definition['connection'][$name]['host'] = 'self';
          $definition['connection'][$name]['endpoint'] = $options['endpoint_' . $name];
        } else {
          foreach (self::XtcServerStruct['env_fields'] as $fieldname) {
            $field = $fieldname . '_' . $name;
            if (!empty($options[$field])) {
              $definition['connection'][$name][$fieldname] = $options[$field];
            }
          }
        }
      }
    }
    return $definition;
  }

}
