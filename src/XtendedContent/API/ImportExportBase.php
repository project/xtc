<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 16:47
 */

namespace Drupal\xtc\XtendedContent\API;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class ImportExportBase {

  /**
   * @param $name
   * @param array $options
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public static function process($name, $options = []) {
    $values = static::load($name, $options);
    $formatted = static::formatValues($values);
    $msg = static::save($formatted, $name, $options);
    return New JsonResponse($msg->getContent(), $msg->getStatusCode(), $msg->headers->all());
  }

  /**
   * @param $name
   * @param array $options
   *
   * @return mixed|null
   */
  protected static function load($name, $options = []) {
    return XtcLoaderProfile::content($name, $options);
  }

  /**
   * @param $values
   *
   * @return mixed
   */
  protected static function formatValues($values) {
    return $values;
  }

  /**
   * @param $formatted
   * @param $name
   * @param array $options
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  protected static function save($formatted, $name, $options = []){
    return New Response('');
  }

}
