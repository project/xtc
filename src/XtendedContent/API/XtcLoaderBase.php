<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 16:47
 */

namespace Drupal\xtc\XtendedContent\API;

/**
 * Class XtcLoaderBase
 *
 * @package Drupal\xtc\XtendedContent\API
 */
abstract class XtcLoaderBase
{

  public static function getList(){
    return \Drupal::service(static::getService())->getDefinitions();
  }

  public static function get($name, $options = []) {
    $plugin = \Drupal::service(static::getService())
                  ->createInstance($name);
    if (!empty($options)) {
      $plugin->setOptions($options);
    }
    return $plugin;
  }

  public static function load($name){
    $definition = static::getDefinition($name);
    if(!empty($definition['override'])){
      $definition = array_merge(self::load($definition['override']), $definition);
    }
    return $definition;
  }

  protected abstract static function getService() : string;

  /**
   * @param $name
   *
   * @return array
   */
  protected static function getDefinition($name): array {
    $loaded = false;
    try {
      $definition = \Drupal::service(static::getService())
          ->getDefinition($name) ?? [];
      $loaded = true;
    } finally {
      if($loaded) {
        return $definition;
      }
      return static::loadXtcEntity($name);
    }
  }

  /**
   * @param $name
   *
   * @return array
   */
  protected static function loadXtcEntity($name): array {
    return [];
  }
}
