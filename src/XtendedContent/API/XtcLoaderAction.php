<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;

class XtcLoaderAction extends XtcLoaderBase {

  public static function get($name, $options = []): XtcActionPluginBase {
    $action = parent::get($name);
    static::init($action, $options);
    return $action;
  }

  protected static function getService(): string {
    return 'plugin.manager.xtc_action';
  }

  protected static function init(XtcActionPluginBase &$action, $options = []) {
    if (!empty($options['name'])) {
      $action->setName($options['name']);;
    }
    if (!empty($options['trigger'])) {
      $action->setTrigger($options['trigger']);;
    }

    if (!empty($options['definition'])) {
      $definition = $options['definition'];
      $action->setDefinition($definition);;

      $profile = $definition['profile'];
      if(!empty($profile)) {
        $profile = XtcLoaderProfile::load($profile);
        $action->setProfile($profile);

        $handler = static::getHandler($profile);
        if(!empty($handler)) {
          $action->setHandler(XtcLoaderHandler::get($handler));
        }
      }

      if(!empty($definition['options'])) {
        $action->setOptions($definition['options']);
      }
    }
  }

  protected static function getHandler($profile) {
    if(!empty($profile['handler'])) {
      return $profile['handler'];
    }
    if(!empty($profile['type']) && !empty($profile['verb'])) {
      return $profile['type'] . '_' . $profile['verb'];
    }
    return '';
  }

}
