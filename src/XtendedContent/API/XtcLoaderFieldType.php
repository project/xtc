<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtc\PluginManager\XtcFieldType\XtcFieldTypePluginBase;

class XtcLoaderFieldType extends XtcLoaderBase {

  /**
   * @param $name
   * @param array $options
   *
   * @return XtcFieldTypePluginBase
   */
  public static function get($name, $options = []): XtcFieldTypePluginBase{
    $fieldType = parent::get($name);
    if ($fieldType instanceof XtcFieldTypePluginBase){
      if (!empty($options)) {
        return $fieldType->setOptions($options);
      }
    }
    return $fieldType;
  }

  /**
   * @return string
   */
  protected static function getService(): string {
    return 'plugin.manager.xtc_fieldtype';
  }

}
