<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtcsearch\XtendedContent\API\XtcSearchLoaderSearch;

/**
 * Class XtcSearch
 *
 * @package Drupal\xtc\XtendedContent\API
 */
class XtcSearch
{

  public static function get($name): array {
    $xtcsearchform = XtcSearchLoaderSearch::get($name);
    return \Drupal::formBuilder()
                  ->getForm($xtcsearchform->getForm());
  }

}
