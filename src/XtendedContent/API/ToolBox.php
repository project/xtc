<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-31
 * Time: 11:59
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Site\Settings;

/**
 * Class ToolBox
 *
 * @package Drupal\xtc\XtendedContent\API
 */
class ToolBox
{

  /**
   * Construction d'une chaine de caractères sans caractère accentué
   *
   * @param string $str chaine à transformer
   *
   * @return string
   */
  public static function replaceAccents($str)
  {
    $accentList = array(
      'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á',
      'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą',
      'ą', 'Ć','ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'É', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'é', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ',
      'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń',
      'ń', 'Ņ', 'ņ','Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť',
      'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ',
      'ǎ', 'Ǐ', 'ǐ', 'Ǒ','ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'
    );
    $noAccentList = array(
      'A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a',
      'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A',
      'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G',
      'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N',
      'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't',
      'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A',
      'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'
    );

    return str_replace($accentList, $noAccentList, $str);
  }

  /**
   * @param $phrase
   *
   * @return mixed
   */
  public static function transliterate($phrase){
    $string = strtolower(\Drupal::transliteration()->transliterate($phrase));
    return str_replace(' ', '_', $string);
  }

  /**
   * @param $phrase
   *
   * @return mixed
   */
  public static function toCssClass($phrase) {
    $string = strtolower(\Drupal::transliteration()->transliterate($phrase));
    return str_replace([' ', '&', '--'], ['-', '', '-'], $string);
  }

  /**
   * @param $prefix
   * @param $id
   *
   * @return string
   */
  public static function buildId($prefix, $id) {
    return $prefix . '_' . $id;
  }


  // Prefix - Suffix

  /**
   * @param $type
   * @param $display
   * @param $name
   *
   * @return string
   */
  public static function getPrefix($type, $display, $name): string {
    $display = XtcSearchLoaderDisplay::load($display);
    return $display[$type][$name]['prefix'] ?? '';
  }

  /**
   * @param $type
   * @param $display
   * @param $name
   *
   * @return string
   */
  public static function getSuffix($type, $display, $name): string {
    $display = XtcSearchLoaderDisplay::load($display);
    return $display[$type][$name]['suffix'] ?? '';
  }

  public static function getPlugins($service, $isFieldable = false) {
    $plugin_definitions = self::getPluginDefinitions($service);
    return static::buildPluginsListFromDefinitions($plugin_definitions);
  }

  /**
   * @return array
   */
  public static function getEntityTypes() {
    $list = [];
    $entityTypes = \Drupal::entityTypeManager()->getDefinitions();
    if (count($entityTypes)) {
      foreach ($entityTypes as $name => $entityType) {
        $provider = $entityType->getProvider();
        $group = $provider;
        $list[$group][$name] = $entityType->getLabel();
      }
    }
    return $list;
  }

  public static function getVerbs() {
    return [
      'get' => 'get',
      'set' => 'set',
      'create' => 'create',
      'touch' => 'touch',
      'update' => 'update',
      'append' => 'append',
      'delete' => 'delete',
      'index' => 'index',
      'unindex' => 'unindex',
    ];
  }
  /**
   * @param $plugin_definitions
   * @param false $isFieldable
   *
   * @return array
   */
  protected static function buildPluginsListFromDefinitions ($plugin_definitions, $isFieldable = false) {
    $plugins = [];
    if (count($plugin_definitions)) {
      foreach ($plugin_definitions as $name => $plugin) {
        $provider = $plugin['provider'];
        $type = (!empty($plugin['type'])) ? ' › ' . $plugin['type'] : '';
        $verb = (!empty($plugin['verb'])) ? ' › ' . $plugin['verb'] : '';
        $group = $provider . $type . $verb;
        if ($isFieldable) {
          if (TRUE == $plugin['fieldable']) {
            $plugins[$group][$name] = $plugin['label'] . ' (' . $name . ')';
          }
        }
        else {
          $plugins[$group][$name] = $plugin['label'] . ' (' . $name . ')';
        }
      }
    }
    return $plugins;
  }

  /**
   * @param $handlerType
   * @param array $verbs
   *
   * @return array
   */
  public static function getProfilesByHandler($handlerType, array $verbs) {
    $profiles = self::getPluginDefinitions('plugin.manager.xtc_profile');
    $allHandlers = self::getPlugins('plugin.manager.xtc_handler');
    $handlers = $allHandlers[$handlerType];

    $plugin_definitions = [];
    foreach ($profiles as $name => $profile) {
      if (!empty($profile['override'])) {
        $overrideProfile = $profiles[$profile['override']];
        $profile['type'] = $overrideProfile['type'];
        $profile['verb'] = $overrideProfile['verb'];
      }
      if(in_array($profile['verb'], $verbs)) {
        $handler = $profile['type'] . '_' . $profile['verb'];
        if (key_exists($handler, $handlers)) {
          $typeVerb = explode('_',$name);
          $group = $typeVerb[0];
          $plugin_definitions[$group][$name] = $profile['label']  . ' (' . $name . ')';
        }
      }
    }
    return $plugin_definitions;
  }

  protected static function loadHandler ($plugin, $handlers) {
    $pluginName = $plugin['type'] . '_' . $plugin['verb'];
    $handler = XtcLoaderHandler::load($pluginName);
    if(in_array($handler['provider'], $handlers)) {
      return $plugin;
    }
  }

  /**
   * @return array
   */
  public static function getModulesList() {
    $list = [];
    $modules = \Drupal::service('extension.list.module')->reset()->getList();
    foreach ($modules as $name => $module) {
      if($module instanceof Extension) {
        $package = $module->info['package'];
        $list[$package][$name] = $module->info['name'];
        ksort($list[$package]);
      }
    }
    ksort($list);
    return $list;
  }

  /**
   * @return array
   */
  public static function getFlySystems() {
    $list = [];
    $flySystems = Settings::get('flysystem');
    foreach ($flySystems as $id => $flySystem) {
      $list['Fly System'][$id] = $flySystem['config']['name'] ?? $id;
    }
    return $list;
  }

  protected static function getPluginDefinitions($service) {
    //Detect all xtc_profile available Plugins
    $type = \Drupal::service($service);
    return $type->getDefinitions();
  }

  public static function splitPipe($options) {
    $params = [];
    $lines = explode("\r\n", $options);
    foreach ($lines as $line) {
      $couple = explode('|', $line);
      $params[$couple[0]] = $couple[1];
    }
    return $params;
  }

  public static function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }
}
