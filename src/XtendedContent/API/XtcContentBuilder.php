<?php

namespace Drupal\xtc\XtendedContent\API;


use Drupal\Core\Entity\EntityBase;

class XtcContentBuilder {


  /**
   * @param \Drupal\Core\Entity\EntityBase $entity
   * @param array $settings
   * @param bool $onlyValue
   *
   * @return mixed
   */
  static public function buildContent(EntityBase $entity, array $settings, bool $onlyValue = false): mixed {
    $content = [];
    if (!empty($settings) && !empty($settings['content'])) {
      foreach ($settings['content'] as $name => $item) {
        switch ($item['type']) {
          case 'field':
            $value = self::buildField($entity, $item);
            break;
          case 'dynamic':
            $fieldNames = $settings['field_names'];
            $bundles = $settings['bundle_names'];
            $name = $fieldNames[$name] ?? $name;
            $value = self::buildDynamic($entity, $item, $fieldNames, $bundles);
            break;
          case 'viewmode':
            $value = self::buildViewMode($entity, $item);
            break;
          default:
            dump('DEFAULT');
        }
        if($onlyValue) {
          $content[$name] = $value['value'];
        }
        else {
          $content[$name] = $value;
        }
      }
    }
    return $content;
  }

  /**
   * @param \Drupal\Core\Entity\EntityBase $entity
   * @param array $settings
   *
   * @return array
   */
  static protected function buildViewMode(EntityBase $entity, array $settings) :array {
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
    $build = $view_builder->view($entity, $settings['name']);
    $value = render($build);
    $content = [
      'type' => $settings['type'],
      'name' => $settings['name'],
      'value' => $value,
    ];
    return $content;
  }

  /**
   * @param \Drupal\Core\Entity\EntityBase $entity
   * @param array $settings
   *
   * @return array
   */
  static public function  buildField(EntityBase $entity, array $settings) : array {
    $content = [];
    /** @var \Drupal\Core\Field\FieldItemList $field */
    $field = (!empty($settings['name'])) ? $entity->get($settings['name']) : null;
    if(!empty($field)) {
      $fieldTypeName = $field->getFieldDefinition()->getType();
      $options = [
        'entity_type' => $entity->getEntityTypeId(),
        'settings' => $settings,
        'field' => $field,
      ];

      /** @var \Drupal\xtcdrupal\Plugin\XtcFieldType\DrupalBase $fieldType */
      $fieldType = XtcLoaderFieldType::get('drupal_' . $fieldTypeName, $options);
      $fieldType->setField();
      $value = $fieldType->formatFrom();
      $content = [
        'type' => $settings['type'],
        'name' => $settings['name'],
        'field_type' => $fieldTypeName,
        'value' => $value,
      ];
    }
    return $content;
  }

  /**
   * @param \Drupal\Core\Entity\EntityBase $entity
   * @param array $settings
   * @param array $fieldNames
   * @param array $bundles
   *
   * @return array
   */
  static public function buildDynamic(EntityBase $entity, array $settings, array $fieldNames, array $bundles) : array {
    $content = [];
    $options = [
      'entity_type' => $entity->getEntityTypeId(),
      'settings' => $settings,
      'field' => $entity->get($settings['name']),
      'field_names' => $fieldNames,
      'bundle_names' => $bundles,
    ];

    /** @var \Drupal\Core\Field\FieldItemList $field */
    $field = (!empty($settings['name'])) ? $entity->get($settings['name']) : null;
    if(!empty($field)) {
      $fieldTypeName = $field->getFieldDefinition()->getType();

      /** @var \Drupal\xtcdrupal\Plugin\XtcFieldType\DrupalBase $xtcFieldType */
      $xtcFieldType = XtcLoaderFieldType::get('drupal_' . $fieldTypeName, $options);
      $xtcFieldType->setField();
      $value = $xtcFieldType->formatFrom();
      $content = [
        'type' => $settings['type'],
        'name' => $settings['name'],
        'field_type' => $fieldTypeName,
        'value' => $value,
      ];
    }
    return $content;
  }

}
