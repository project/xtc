<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtc\PluginManager\XtcTrigger\XtcTriggerDefault;

class XtcLoaderTrigger extends XtcLoaderBase {

  public static function get($name, $options = []): XtcTriggerDefault {
    return parent::get($name, $options);
  }

  protected static function getService(): string {
    return 'plugin.manager.xtc_trigger';
  }

}
