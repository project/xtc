<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-01-29
 * Time: 17:08
 */

namespace Drupal\xtc\XtendedContent\API;


use Drupal\xtcsearch\PluginManager\XtcSearchFilterType\XtcSearchFilterTypePluginBase;

class XtcSearchLoaderFilterType extends XtcLoaderBase
{

  public static function get($name, $options = []): XtcSearchFilterTypePluginBase{
    return parent::get($name);
  }

  protected static function getService() : string {
    return 'plugin.manager.xtcsearch_filter_type';
  }

  public static function loadFromFilter($name) : XtcSearchFilterTypePluginBase{
    $filter = XtcSearchLoaderFilter::get($name);
    return $filter->getFilterType();
  }

}
