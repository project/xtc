<?php

namespace Drupal\xtc\XtendedContent\Interfaces;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

interface DeleteInterface {

  /**
   * @return XtcHandlerPluginBase
   */
  public function deleteContent() : XtcHandlerPluginBase;

}
