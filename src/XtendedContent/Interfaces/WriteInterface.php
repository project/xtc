<?php

namespace Drupal\xtc\XtendedContent\Interfaces;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

interface WriteInterface {

  /**
   * @param $content
   *
   * @return XtcHandlerPluginBase
   */
  public function writeContent($content): XtcHandlerPluginBase;

}
