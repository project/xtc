<?php

namespace Drupal\xtc\PluginManager\XtcTrigger;

use Drupal\Component\Plugin\Definition\PluginDefinitionInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\Core\Plugin\Factory\ContainerFactory;

/**
 * Defines a plugin manager to deal with xtc_triggers.
 *
 * Modules can define xtc_triggers in a MODULE_NAME.xtc_triggers.yml file contained
 * in the module's base directory. Each xtc_trigger has the following structure:
 *
 * @code
 *   MACHINE_NAME:
 *     label: STRING
 *     description: STRING
 * @endcode
 *
 * @see \Drupal\xtc\PluginManager\XtcTrigger\XtcTriggerDefault
 * @see \Drupal\xtc\PluginManager\XtcTrigger\XtcTriggerInterface
 * @see plugin_api
 */
class XtcTriggerPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  protected $defaults = [
    // The xtc_trigger id. Set by the plugin system based on the top-level YAML key.
    'id' => '',
    // The xtc_trigger label.
    'label' => '',
    // The xtc_trigger description.
    'description' => '',
    // Default plugin class.
    'class' => 'Drupal\xtc\PluginManager\XtcTrigger\XtcTriggerDefault',
  ];

  /**
   * Constructs XtcTriggerPluginManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->factory = new ContainerFactory($this);
    $this->moduleHandler = $module_handler;
    $this->alterInfo('xtc_trigger_info');
    $this->setCacheBackend($cache_backend, 'xtc_trigger_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    // If this PluginManager has fallback capabilities catch
    // PluginNotFoundExceptions.
    if ($this instanceof FallbackPluginManagerInterface) {
      try {
        return $this->getFactory()->createInstance($plugin_id, $configuration);
      }
      catch (PluginNotFoundException $e) {
        return $this->handlePluginNotFound($plugin_id, $configuration);
      }
    }
    else {
      return $this->getFactory()->createInstance($plugin_id, $configuration);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('xtc_triggers', $this->moduleHandler->getModuleDirectories());
      $this->discovery->addTranslatableProperty('label', 'label_context');
      $this->discovery->addTranslatableProperty('description', 'description_context');
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    // Only array-based definitions can have defaults merged in.
    if (is_array($definition) && !empty($this->defaults) && is_array($this->defaults)) {
      $definition = NestedArray::mergeDeep($this->defaults, $definition);
    }
    if(!empty($definition['type'])) {
      $class = \Drupal::getContainer('xtcaction.' . $definition['type']);
      $definition->setClass($class);
    }

    // Keep class definitions standard with no leading slash.
    if ($definition instanceof PluginDefinitionInterface) {
      $definition->setClass(ltrim($definition->getClass(), '\\'));
    }
    elseif (is_array($definition) && isset($definition['class'])) {
      $definition['class'] = ltrim($definition['class'], '\\');
    }
  }

}
