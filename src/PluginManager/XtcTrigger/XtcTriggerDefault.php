<?php

namespace Drupal\xtc\PluginManager\XtcTrigger;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\xtc\PluginManager\XtcPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderAction;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtc\XtendedContent\API\XtcLoaderTrigger;
use Drupal\xtcfile\XtendedContent\API\WriterFile;

/**
 * Default class used for xtc_triggers plugins.
 */
class XtcTriggerDefault extends XtcPluginBase implements XtcTriggerInterface {

  /**
   * @var ContentEntityBase
   */
  var $entity;

  /**
   * @var ContentEntityBase
   */
  var $currentEntity;

  /**
   * @var \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  var $handler;

  var $profile;

  var $settings = [];

  /**
   * {@inheritdoc}
   */
  public function label() {
    // The title from YAML file discovery may be a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * @param $content_entity_type
   * @param $content
   *
   * @return array[]
   */
  public function buildContent($content_entity_type, $content) {
    $this->loadEntity($content_entity_type, $content);
    return $this->build();
  }

  /**
   * @return array[]
   */
  public function build() {
    $response = [
      'input' => [],
      'response' => [],
    ];
    $this->settings = array_merge_recursive($this->getPluginDefinition(), $this->options);
    if($this->controlRequirements()) {
      $this->process($response);
    }
    return $response;
  }

  public function process(&$response) {
    $actions = $this->settings['actions'];
    foreach ($actions as $name) {
      $definition = $this->settings['definitions'][$name];
      $type = $definition['type'];
      if(!empty($type)) {
        $options = [
          'name' => $name,
          'trigger' => $this,
          'definition' => $definition,
        ];
        $action = XtcLoaderAction::get($type, $options);
        $action->process($response);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    if(!empty($this->pluginDefinition['override'])){
      $this->pluginDefinition = array_merge(XtcLoaderTrigger::load($this->pluginDefinition['override']), $this->pluginDefinition);
    }
    return $this->pluginDefinition;
  }

  protected function getHandler($profile) {
    if(!empty($profile['handler'])) {
      return $profile['handler'];
    }
    if(!empty($profile['type']) && !empty($profile['verb'])) {
      return $profile['type'] . '_' . $profile['verb'];
    }
    return '';
  }

  protected function write($content, $definition) {
    $fileName = $this->getFullName($definition['location']);
    $writer = XtcLoaderProfile::load($definition['profile']);
    $options = array_merge($writer, ['filename' => $fileName]);
    return WriterFile::write($content, $definition['profile'], $options);
  }

  protected function loadEntity($content_entity_type, $content) {
    $this->entity = \Drupal::service('entity_type.manager')
      ->getStorage($content_entity_type)
      ->load($content);
    $this->currentEntity = $this->entity;
  }

  /**
   * @return bool
   */
  protected function controlRequirements(): bool {
    $entity = FALSE;
    $bundle = FALSE;
    $requirements = $this->settings['requirements'] ?? [];
    if(empty($requirements)) {
      return TRUE;
    }
    if (
      !empty($requirements['entity_type']) &&
      $requirements['entity_type'] == $this->entity->getEntityTypeId()
    ) {
      $entity = TRUE;
    }
    if (
      !empty($requirements['bundles']) &&
      in_array($this->entity->bundle(), $requirements['bundles'])
    ) {
      $bundle = TRUE;
    }
    return ($entity && $bundle);
  }

  protected function getFullName(array $settings) {
    $entity = $this->currentEntity;
    if(!empty($settings['fullname'])) {
      $fullName = '';
      if (empty($settings['fullname_type'])) {
        $settings['fullname_type'] = 'value';
      }
      switch ($settings['fullname_type']) {
        case 'field':
          $pathField = $entity->get($settings['fullname']);
          if (!empty($pathField->first())) {
            $fullName = $pathField->first()->getValue()['value'] ?? $fullName;
          }
          break;
        case 'value':
        default:
          $fullName = $settings['fullname'];
      }
      return $fullName;
    }

    $path = '';
    $filename = '';
    if (!empty($settings['path'])) {
      if (empty($settings['path_type'])) {
        $settings['path_type'] = 'value';
      }
      switch ($settings['path_type']) {
        case 'field':
          $pathField = $entity->get($settings['path']);
          if (!empty($pathField->first())) {
            $path = $pathField->first()->getValue()['value'] ?? $path;
          }
          break;
        case 'value':
        default:
          $path = $settings['path'];
      }
    }
    if (!empty($settings['name'])) {
      if (empty($settings['name_type'])) {
        $settings['name_type'] = 'static';
      }
      switch ($settings['name_type']) {
        case 'field':
          $pathField = $entity->get($settings['name']);
          $filename = $pathField->first()->getValue()['value'];
          break;
        case 'static':
        default:
          $filename = $settings['name'];
      }
    }
    return $this->glueFullname($path, $filename);
  }

  /**
   * @param $path
   * @param $filename
   *
   * @return string
   */
  protected function glueFullname($path, $filename) {
    $fullName = '';
    if (!empty($path) && !empty($filename)) {
      $fullName = $path . '/' . $filename;
    }
    else {
      if (!empty($path)) {
        $fullName = $path;
      }
      elseif (!empty($filename)) {
        $fullName = $filename;
      }
    }
    return $fullName;
  }

}
