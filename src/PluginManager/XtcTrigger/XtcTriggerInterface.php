<?php

namespace Drupal\xtc\PluginManager\XtcTrigger;

/**
 * Interface for xtc_trigger plugins.
 */
interface XtcTriggerInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

}
