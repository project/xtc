<?php

namespace Drupal\xtc\PluginManager\XtcProfile;

use Drupal\xtc\PluginManager\XtcPluginBase;

/**
 * Default class used for xtc_profiles plugins.
 */
class XtcProfileDefault extends XtcPluginBase implements XtcProfileInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // The title from YAML file discovery may be a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
