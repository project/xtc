<?php

namespace Drupal\xtc\PluginManager\XtcServer;

use Drupal\xtc\PluginManager\XtcPluginBase;

/**
 * Default class used for xtc_servers plugins.
 */
class XtcServerDefault extends XtcPluginBase implements XtcServerInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // The title from YAML file discovery may be a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
