<?php

namespace Drupal\xtc\PluginManager\XtcRequest;

use Drupal\xtc\PluginManager\XtcPluginBase;

/**
 * Default class used for xtc_requests plugins.
 */
class XtcRequestDefault extends XtcPluginBase implements XtcRequestInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // The title from YAML file discovery may be a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
