<?php

namespace Drupal\xtc\PluginManager\XtcCache;

use Drupal\xtc\PluginManager\XtcPluginBase;

/**
 * Base class for XTC Cache plugins.
 */
abstract class XtcCachePluginBase extends XtcPluginBase implements XtcCacheInterface {

  /**
   * @var mixed
   */
  protected $content;

  /**
   * XtcCachePluginBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * @return mixed
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * @return bool
   */
  public function exists(): bool {
    $cachedContent = $this->read();
    if (!empty($cachedContent)) {
      $this->content = $cachedContent;
      return TRUE;
    }
    return FALSE;
  }

  public function read() {

  }

  public function write() {

  }

}
