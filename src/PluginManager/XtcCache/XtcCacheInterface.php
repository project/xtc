<?php

namespace Drupal\xtc\PluginManager\XtcCache;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for XTC Cache plugins.
 */
interface XtcCacheInterface extends PluginInspectionInterface {

  /**
   * @return bool
   */
  public function exists() : bool;

  public function write();

  public function read();

}
