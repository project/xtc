<?php

namespace Drupal\xtc\PluginManager\XtcHandler;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for xtc_handler plugins.
 */
interface XtcHandlerInterface extends PluginInspectionInterface
{

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  public static function type();

  public function setProfile(array $profile) : XtcHandlerPluginBase;

  public function setOptions($options = []);

  public static function getStruct(): array;

}
