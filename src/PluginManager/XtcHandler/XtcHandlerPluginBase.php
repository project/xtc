<?php

namespace Drupal\xtc\PluginManager\XtcHandler;

use Drupal\Component\Serialization\Json;
use Drupal\xtc\PluginManager\XtcPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtc\PluginManager\XtcCache\XtcCachePluginBase;
use Drupal\xtc\XtendedContent\Interfaces\WriteInterface;

/**
 * Base class for xtc_handler plugins.
 */
abstract class XtcHandlerPluginBase extends XtcPluginBase implements XtcHandlerInterface, WriteInterface {

  /**
   * @var string
   */
  protected $type;

  /**
   * @var string
   */
  protected $method;

  /**
   * @var string
   */
  protected $param;

  /**
   * @var mixed
   */
  protected $content;

  /**
   * @var XtcCachePluginBase
   */
  protected $cache;

  /**
   * @var array
   */
  protected $profile = [];

  /**
   * XtcHandlerPluginBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->type = $this->type();
    $this->cache = $this->type();
  }

  /**
   * @return string
   */
  public static function type() {
    return 'other';
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * @return mixed|null
   */
  public function processContent() {
    return $this->process()
      ->content();
  }

  /**
   * @return mixed|null
   */
  public function content() {
    return $this->content ?? NULL;
  }

  /**
   * @return $this
   */
  public function process() {
    if (!empty($this->profile['cache']) && $this->cacheExists()) {
      return $this;
    }
    $this->initProcess();
    $this->runProcess();
    $this->adaptContent();
    if (!empty($this->profile['cache'])) {
      $this->writeToCache();
    }
    return $this;
  }

  /**
   * @return bool
   */
  protected function cacheExists() {
    $cachedContent = $this->readFromCache();
    if (!empty($cachedContent)) {
      $this->content = $cachedContent;
      return TRUE;
    }
    return FALSE;
  }

  protected function writeToCache() {
    $cache_profile = $this->profile['cache'];
    $cache_profile['id'] = $this->profile['id'] . '_write_cache';
    $cache_profile['label'] = $this->profile['label'] . ' - Write to Cache';
    $cache_profile['description'] = $this->profile['description'] . ' - Write to Cache';
    $cache_profile['type'] = 'file_set';
    $jsonContent = Json::encode($this->content);

    if ('dir' == $this->profile['cache']['type']) {
      $filename = (!empty($cache_profile['filetype']))
        ? $this->profile['id'] . '.' . $cache_profile['filetype']
        : $this->profile['id'];
      $cache_profile['path'] = $cache_profile['path'] . '/' . $filename;
    }

    if ((!empty($cache_profile)) && (!empty($this->profile['cache']['type']))) {
      $handler = XtcLoaderHandler::get($cache_profile['type']);
      $handler->setProfile($cache_profile)
        ->setOptions($this->options)
        ->setContent($jsonContent);
      $handler->process();
    }
  }

  protected function readFromCache() {
    $cache_profile = $this->profile['cache'];
    $cache_profile['id'] = $this->profile['id'] . '_read_cache';
    $cache_profile['label'] = $this->profile['label'] . ' - Read from Cache';
    $cache_profile['description'] = $this->profile['description'] . ' - Read from Cache';
    $cache_profile['type'] = $cache_profile['filetype'] . '_get';
    if ('dir' == $this->profile['cache']['type']) {
      $filename = (!empty($cache_profile['filetype']))
        ? $this->profile['id'] . '.' . $cache_profile['filetype']
        : $this->profile['id'];
      $cache_profile['path'] = $cache_profile['path'] . '/' . $filename;
    }

    if ((!empty($cache_profile)) && (!empty($cache_profile['filetype']))) {
      $handler = XtcLoaderHandler::get($cache_profile['type']);
      $cachedContent = $handler->setProfile($cache_profile)
        ->setOptions($this->options)
        ->processContent();
      return ('json' == $cache_profile['filetype']) ? Json::decode($cachedContent) : $cachedContent;
    }
    return NULL;
  }

  protected function initProcess() {
  }

  protected function runProcess() {
  }

  protected function adaptContent() {
  }

  /**
   * @return mixed|null
   */
  public function processValues() {
    return $this->process()
      ->values();
  }

  /**
   * @return mixed|null
   */
  public function values() {
    return $this->content['values'] ?? NULL;
  }

  /**
   * @param $content
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public function writeContent($content): XtcHandlerPluginBase {
    return $this->setContent($content)
      ->process();
  }

  /**
   * @param $content
   *
   * @return $this
   */
  public function setContent($content) {
    $this->content = $content;
    return $this;
  }

  /**
   * @param array $profile
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public function setProfile(array $profile): XtcHandlerPluginBase {
    $this->profile = $profile;
    return $this;
  }

  /**
   * @param        $method
   * @param string $param
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public function init($method, $param = ''): XtcHandlerPluginBase {
    $this->method = $method;
    $this->param = $param;
    return $this;
  }

  /**
   * @return string[]
   */
  public static function getStruct(): array {
    return [];
  }

}
