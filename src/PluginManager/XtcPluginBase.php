<?php

namespace Drupal\xtc\PluginManager;

abstract class XtcPluginBase extends \Drupal\Core\Plugin\PluginBase {

  /**
   * @var array
   */
  var $options = [];


  /**
   * @return array
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * @param $name
   *
   * @return mixed|null
   */
  public function getOption($name) {
    return $this->options[$name] ?? null;
  }

  /**
   * @param array $options
   *
   * @return $this
   */
  public function setOptions($options = []) {
    $this->options = $options;
    return $this;
  }

}
