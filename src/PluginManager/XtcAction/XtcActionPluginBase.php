<?php

namespace Drupal\xtc\PluginManager\XtcAction;

use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;
use Drupal\xtc\PluginManager\XtcPluginBase;
use Drupal\xtc\PluginManager\XtcTrigger\XtcTriggerDefault;
use Drupal\xtc\XtendedContent\API\XtcLoaderAction;
use Drupal\xtc\Plugin\XtcAction\Entity as XtcActionEntity;



/**
 * Base class for xtc_action plugins.
 */
abstract class XtcActionPluginBase extends XtcPluginBase implements XtcActionInterface {

//  /**
//   * @var ContentEntityBase
//   */
//  var $entity;
//
//  /**
//   * @var ContentEntityBase
//   */
//  var $currentEntity;

  /**
   * @var \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  var $handler;

  /**
   * @var array
   */
  var $definition = [];

  /**
   * @var array
   */
  var $profile = [];

  /**
   * @var XtcTriggerDefault
   */
  var $trigger;

  var $name;

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * @param $response
   */
  public function process(&$response) {
  }

  /**
   * @param $name
   *
   * @return $this
   */
  public function setName($name): XtcActionPluginBase {
    $this->name = $name;
    return $this;
  }

  /**
   * @param array $definition
   *
   * @return $this
   */
  public function setDefinition($definition = []): XtcActionPluginBase {
    $this->definition = $definition;
    return $this;
  }

  /**
   * @param array $profile
   *
   * @return $this
   */
  public function setProfile(array $profile): XtcActionPluginBase {
    $this->profile = $profile;
    return $this;
  }

  /**
   * @param \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase $handler
   *
   * @return $this
   */
  public function setHandler(XtcHandlerPluginBase $handler): XtcActionPluginBase {
    $this->handler = $handler;
    return $this;
  }

  /**
   * @param \Drupal\xtc\PluginManager\XtcTrigger\XtcTriggerDefault $trigger
   *
   * @return $this
   */
  public function setTrigger(XtcTriggerDefault $trigger): XtcActionPluginBase {
    $this->trigger = $trigger;
    return $this;
  }

  protected function getFullName(array $settings) {
    $action = $settings[$settings['actionName']];
    $fullName = '';
    /** @var \Drupal\Core\Entity\EntityBase $entity */
    $entity = $settings['currentEntity'];
    $bundle = $entity->bundle();
    $currentSettings = $action['options']['bundles'][$bundle];

    $actionTypes = XtcLoaderAction::getList();
    if(key_exists($action['type'], $actionTypes)) {
      $class = $actionTypes[$action['type']]['class'];
      if (is_subclass_of($class, XtcActionEntity::class) ) {
        $location = $currentSettings['location'][$settings['name']];
        $fullName = '';
        switch ($location['type']) {
          case 'field':
            $fieldName = $location['fullname'];
            $field = $entity->get($fieldName);
            if (!empty($field->first())) {
              $fullName = $field->first()->getValue()['value'] ?? '';
            }
            break;
          case 'fullname_pattern':
            foreach ($location['pattern'] as $part) {
              if ('string' == $part['type']) {
                $fullName .= $part['value'] ?? '';
              }
              if ('arg' == $part['type']) {
                $fullName .= $settings['args'][$part['value']] ?? '';
              }
            }
            break;
          default:
        }
      }
    }

    return $fullName;
  }

  /**
   * @param $path
   * @param $filename
   *
   * @return string
   */
  protected function glueFullname($path, $filename) {
    $fullName = '';
    if (!empty($path) && !empty($filename)) {
      $fullName = $path . '/' . $filename;
    }
    else {
      if (!empty($path)) {
        $fullName = $path;
      }
      elseif (!empty($filename)) {
        $fullName = $filename;
      }
    }
    return $fullName;
  }

}
