<?php

namespace Drupal\xtc\PluginManager\XtcAction;

/**
 * Interface for xtc_action plugins.
 */
interface XtcActionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

}
