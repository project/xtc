<?php

namespace Drupal\xtc\PluginManager\XtcAction;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * XtcAction plugin manager.
 */
class XtcActionPluginManager extends DefaultPluginManager {

  /**
   * Constructs XtcActionPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/XtcAction',
      $namespaces,
      $module_handler,
      'Drupal\xtc\PluginManager\XtcAction\XtcActionInterface',
      'Drupal\xtc\Annotation\XtcAction'
    );
    $this->alterInfo('xtc_action_info');
    $this->setCacheBackend($cache_backend, 'xtc_action_plugins');
  }

}
