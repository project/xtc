<?php

namespace Drupal\xtc\PluginManager\XtcFieldType;

use Drupal\xtc\PluginManager\XtcPluginBase;

/**
 * Base class for xtc_fieldtype plugins.
 */
abstract class XtcFieldTypePluginBase extends XtcPluginBase implements XtcFieldTypeInterface {

  /**
   * XtcFieldTypePluginBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

}
