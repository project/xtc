<?php

namespace Drupal\xtc\PluginManager\XtcFieldType;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * XtcFieldType plugin manager.
 */
class XtcFieldTypePluginManager extends DefaultPluginManager {

  /**
   * Constructs XtcFieldTypePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/XtcFieldType',
      $namespaces,
      $module_handler,
      'Drupal\xtc\PluginManager\XtcFieldType\XtcFieldTypeInterface',
      'Drupal\xtc\Annotation\XtcFieldType'
    );
    $this->alterInfo('xtc_fieldtype_info');
    $this->setCacheBackend($cache_backend, 'xtc_fieldtype_plugins');
  }

}
