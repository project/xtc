<?php

namespace Drupal\xtc\PluginManager\XtcFieldType;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for xtc_fieldtype plugins.
 */
interface XtcFieldTypeInterface extends PluginInspectionInterface
{
  public function setField() : XtcFieldTypeInterface;

}
