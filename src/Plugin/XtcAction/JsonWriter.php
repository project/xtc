<?php


namespace Drupal\xtc\Plugin\XtcAction;


use Drupal\Component\Serialization\Json;
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcfile\XtendedContent\API\WriterFile;

/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "jsonwriter",
 *   label = @Translation("Json Writer for XTC Action"),
 *   description = @Translation("Json Writer for XTC Action description.")
 * )
 */
class JsonWriter extends Writer {

  protected function adaptContent (&$content) {
    $content = Json::encode($content);
  }

}
