<?php


namespace Drupal\xtc\Plugin\XtcAction;


use Drupal\Core\Entity\EntityBase;
use Drupal\xtc\Plugin\XtcHandler\TouchGet;
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;
use Drupal\xtc\XtendedContent\API\XtcContentBuilder;
use Drupal\xtc\XtendedContent\API\XtcLoaderFieldType;

/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "entity",
 *   label = @Translation("Get Entity for XTC Action"),
 *   description = @Translation("Get Entity for XTC Action description.")
 * )
 */
class Entity extends Get {

  /**
   * @var \Drupal\Core\Entity\ContentEntityBase
   */
  var $entity;

  /**
   * @param $name
   *
   * @return mixed|null
   */
  protected function treat($name) {
    $input = parent::treat($name);
    if (!empty($input)) {
      /**
       * @var  $id
       * @var \Drupal\Core\Entity\EntityBase $entity
       */
      foreach ($input as $id => $entity) {
        $this->entity = $entity;
        $input[$id] = [];

        $options = $this->getOption('bundles');
        $settings = $options[$entity->bundle()] ?? [];
        $input[$id]['values'] =  XtcContentBuilder::buildContent($entity, $settings);
        $input[$id]['entity'] = $entity;
      }
    }
    return $input;
  }

}
