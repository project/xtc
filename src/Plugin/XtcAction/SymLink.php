<?php


namespace Drupal\xtc\Plugin\XtcAction;


/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "symlink",
 *   label = @Translation("Symbolic Link for XTC Action"),
 *   description = @Translation("Symbolic Link for XTC Action description.")
 * )
 */

use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderHandler;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;

class SymLink extends XtcActionPluginBase {

  /**
   * @param $response
   */
  public function process(&$response) {
    $name = $this->name;
    $definition = $this->definition;

    $fileName = $this->getFullName($definition['options']['full_link']);
//    $fileName = $this->trigger . '/' . $this->getFullName($definition['options']['full_link']);
    $profile = XtcLoaderProfile::load($definition['profile']);
    $handler = XtcLoaderHandler::get($profile['handler']);
    $handler->setProfile($profile);
    $handler->setOptions(array_merge($profile, ['filename' => $fileName]));
    $options = $handler->getOptions();
    $settings = $definition['options'] ?? NULL;
    $target = $this->getFullName($settings['target']) ?? NULL;
    $fullLink = $options['path'];
    if(!empty($target) && !empty($fullLink)) {
      $response['response'][$name] = symlink($target, $fullLink);
    }
  }

}
