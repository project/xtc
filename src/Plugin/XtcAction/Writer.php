<?php


namespace Drupal\xtc\Plugin\XtcAction;


use Drupal\Component\Serialization\Json;
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;
use Drupal\xtc\XtendedContent\API\XtcLoaderProfile;
use Drupal\xtcfile\XtendedContent\API\WriterFile;

/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "writer",
 *   label = @Translation("Writer for XTC Action"),
 *   description = @Translation("Writer for XTC Action description.")
 * )
 */
class Writer extends XtcActionPluginBase {

  /**
   * @param $response
   */
  public function process(&$response) {
    $name = $this->name;
    $definition = $this->definition;
    $action = $definition['action'];
    $contentField = $definition['content']['field'] ?? '';
    $locationField = $definition['location']['field'] ?? '';

    if(!empty($action)) {
      if (!empty($response['input'][$action])) {
        $contents = $response['input'][$action];
        if(!empty($definition['multiple']) && !empty($contents)) {
          foreach ($contents as $key => $content) {
            $this->treatItem($response, $name, $content, $key, $definition);
          }
        }
        elseif (!empty($contentField)) {
          $def = $definition;
          $def['location'] = $contents[$locationField]['location'];
          $response['response'][$name] = $this->write($contents[$contentField], $def);
        }
        else {
          $response['response'][$name] = $this->write($contents, $definition);
        }
      }
      else {
        $response['response'][$name] = $this->write('', $definition);
      }
    }
  }

  protected function treatItem(&$response, $name, $content, $key, $definition) {
    $entity = $content['entity'] ?? NULL;
    $triggerDefinition = $this->trigger->getPluginDefinition();
    $actions = $triggerDefinition['definitions'];
    $actionName = $definition['action'];
    $action = $actions[$actionName];

    switch($definition['location']['type']) {
      case 'field':
      case 'pattern':
        $definition['location']['args']['id'] = $key;
        $definition['location']['actionName'] = $actionName;
        $definition['location']['currentEntity'] = $entity;
        $definition['location'][$actionName] = $action;
        break;
      default:
    }

    foreach ($definition['content'] as $contentName) {
      $value = $content['values'][$contentName] ?? '';
      $item[$contentName] = $value['value'] ?? '';
    }
    if (!empty($item['root'])) {
      $item = $item['root'];
    }
    $this->adaptContent($item);
    $response['response'][$name][$key] = $this->write($item, $definition);
  }

  protected function adaptContent (&$content) {
  }

  protected function write($content, $definition) {
    $fileName = $this->getFullName($definition['location']);
    $writer = XtcLoaderProfile::load($definition['profile']);
    $options = array_merge($writer, ['filename' => $fileName]);
    return WriterFile::write($content, $definition['profile'], $options);
  }

}
