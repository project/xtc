<?php


namespace Drupal\xtc\Plugin\XtcAction;


/**
 * Plugin implementation of the xtc_action.
 *
 * @XtcAction(
 *   id = "get",
 *   label = @Translation("Get for XTC Action"),
 *   description = @Translation("Get for XTC Action description.")
 * )
 */

use Drupal\xtc\Plugin\XtcHandler\TouchGet;
use Drupal\xtc\PluginManager\XtcAction\XtcActionPluginBase;

class Get extends XtcActionPluginBase {

  /**
   * @param $response
   */
  public function process(&$response) {
    $name = $this->name;
    $definition = $this->definition;
    $action = $definition['action'] ?? '';

    if(!empty($action) && !empty($response['input'][$action])) {
      $contents = $response['input'][$action];
      if(!empty($definition['multiple'])) {
        foreach ($contents as $key => $content) {
          $val = $this->treatItem($response, $name, $content);
          if(!empty($val)) {
            if(is_array($val)) {
              foreach ($val as $id => $value) {
                $response['input'][$name][$key.'-'.$id] = $value;
              }
            }
            else {
              $response['input'][$name][$key] = $val;
            }
          }
        }
      }
    } else {
      if (!empty($this->profile)) {
        $response['input'][$name] = $this->treat($name);
      }
    }
    $this->adaptContent($response);
  }

  protected function adaptContent(&$response) {
  }

  protected function treatItem(&$response, $name, $content) {
//    $this->currentEntity = $content['entity'];
    if(!empty($this->options['fieldname'])) {
      if (!empty($this->profile)) {
        $this->profile['entity_type_id'] = $content['entity_type'];
        $this->profile['content_id'] = $content['target_id'];
        return $this->treat($name);
      }
    }
  }

  /**
   * @param $name
   *
   * @return mixed|null
   */
  protected function treat($name) {
    $input = NULL;
    $profile = $this->profile;
    if(!empty($this->options)) {
      $profile = array_merge($profile, $this->options);
    }
    $handler = $this->handler;
    $handler->setProfile($profile);
    $handler->setOptions($profile);
    $content = $handler->processContent();
    if(!empty($content) || ($handler instanceof TouchGet)) {
      $input = $content;
    }
    return $input;
  }

}
