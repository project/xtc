<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtc\Plugin\XtcHandler;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "dummy_get",
 *   label = @Translation("Dummy Get for XTC"),
 *   description = @Translation("Dummy Get for XTC description.")
 * )
 */
class DummyGet extends XtcHandlerPluginBase {

}
