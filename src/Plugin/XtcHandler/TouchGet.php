<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtc\Plugin\XtcHandler;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "touch_get",
 *   label = @Translation("Touch file Get for XTC"),
 *   description = @Translation("Touch file Get for XTC description.")
 * )
 */
class TouchGet extends XtcHandlerPluginBase {

  /**
   * @return mixed|null
   */
  public function content() {
    return '';
  }

}
