<?php

namespace Drupal\xtc\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a XTC Cache item annotation object.
 *
 * @see \Drupal\xtc\Plugin\XtcCacheManager
 * @see plugin_api
 *
 * @Annotation
 */
class XtcCache extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
